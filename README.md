﻿Made by:
Nataniel Gåsøy
Christian Bråthen Tverberg
Maarten Dijkstra

For this project we have made an open world RPG, based on a remote island. While there is a lot of area on the island, there is not that much content around the island itself. 
All the exciting things except the actual island and vegetation itself is located around the spawn position.
This includes:
* Enemy AI (using a finite state machine)
* Multiplayer functionality
* UI (Minimap, inventory, health and mana etc.)
* A quest system

