﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/WaterShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "blue" {}		// Water texture
		_TintColor("Tint Color", Color) = (1, 1, 1, 1)	
		_FoamColor("Foam Color", Color) = (1, 1, 1, 1)
		_WaterColor("Water Color", Color) = (1, 1, 1, 1)
		_Speed("Speed", Range(0.0, 5.0)) = 0.5	
		_Height("Height", Range(0.0, 1.0)) = 0.25
		_AmountX("Wave Amount X", Range(-6.0, 6.0)) = 0.5
		_AmountZ("Wave Amount Z", Range(-6.0, 6.0)) = 0.5
		_Fade("Fade Length", Range(0, 2)) = 0.15
		_Cube ("Reflection Map", Cube) = "" {}	// Supposed to give reflection, doesn't do it atm

		_ColPosition("Collision Position", Vector) = (0, 0, 0)
		_Direction0("Direction", Vector) = (0, 0, 0, 0)
		_Frequency("Frequency", Range(0, 1)) = 0.5
		_Amplitude("Amplitude", Range(0, 1)) = 0.5
		_Steepness("Steepness", Range(0, 1)) = 0.5
		_NumOfWaves("Number of waves", Int) = 1

	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 100


		ZWrite On
		Blend srcAlpha OneMinusSrcAlpha




		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			// make fog work
			//#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _TintColor;
			float4 _FoamColor;
			float4 _WaterColor;
			float _Transparency;
			float _Speed;
			float _Height;
			float _AmountX;
			float _AmountZ;
			float4 _AlphaColor;
			uniform samplerCUBE _Cube;
			float3 _ColPosition;

			//Testing stuff again:(This is not used in final product atm.)
			float4 _Direction;
			float _Frequency;
			float _Amplitude;
			float _Steepness;
			int _NumOfWaves;


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 screenPos : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				float4 pos : TEXCOORD4;
				float3 tangent : TEXCOORD5;
				float3 bitangent : TEXCOORD6;
				float3 worldPos : TEXCOORD7;
				//float3 ray : TEXCOORD3;
			};
			
			// We want Gerstner waves (not used in final product atm)
			#define ADD_WAVE(inputPos, W, posOut, tangentOut, bitangentOut) \
			{ \
			float2 direction = normalize(_Direction.xy);	\
				float time = _Speed * _Time.y; \
				float progress = dot(_Frequency * direction, inputPos) + time; \
				float normS = sin(_Frequency * dot(direction, inputPos) + time); \
				float normC = cos(_Frequency * dot(direction, inputPos) + time); \
				float FA = _Frequency * _Amplitude; \
				float steepness = FA == 0 ? 0 : _Steepness / (FA * _NumOfWaves); \
				posOut += float3(steepness * _Amplitude * direction.x * cos(progress), \
					_Amplitude * sin(progress), \
					steepness * _Amplitude * direction.y * cos(progress)); \
				tangentOut += float3(steepness * direction.x * direction.y * FA * normS, \
					direction.y * FA * normC, \
					steepness * direction.y * direction.y * FA * normS); \
				bitangentOut += float3(steepness * direction.x * direction.x * FA * normS, \
					direction.x * FA * normC, \
					steepness * direction.x * direction.y * FA * normS); \
			}


			// (not used in final product atm)
			#define WAVE_FINAL_TANGENTS(tangentOut, bitangentOut) \
			{ \
				tangentOut = normalize(float3(-tangentOut.x, tangentOut.y, 1 - tangentOut.z)); \
				bitangentOut = normalize(float3(1 - bitangentOut.x, bitangentOut.y, -bitangentOut.z)); \
			}

			
			v2f vert (appdata v, out float4 vertex : SV_POSITION)
			{
				v2f o;
				//float rnd8 = 2*frac(cos(12345678.+ 1024. * dot(v.uv, 2)));
				//v.vertex.y += sin(_Time.z * _Speed + (v.vertex.x * _AmountX * rnd8 + v.vertex.z * _AmountZ * rnd8)) * _Height;
				//v.vertex.y += v.vertex.x - sin(_Speed * _Time.z + (v.vertex.x));
				//v.vertex.y += sin(_Time.w * _Speed + (_ColPosition.x - v.vertex.x) * _AmountX) * _Height;
				
				// Gerstner wave stuff:	(not used in final product atm.)
				/*o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				float3 waveOffset = float3(0, 0, 0);
				o.tangent = float3(0, 0, 0);
				o.bitangent = float3(0, 0, 0);

				for (int i = 0; i < _NumOfWaves; i++)
				{
					ADD_WAVE(o.worldPos.xz, i, waveOffset, o.tangent, o.bitangent);
				}

				WAVE_FINAL_TANGENTS(o.tangent, o.bitangent);
				o.worldPos += waveOffset;

				v.vertex.y += sin(_Amplitude * cos(o.worldPos) + _Time.y);
				v.vertex.x += cos(_Amplitude * cos(o.worldPos) + _Time.y);*/


				/*o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.ray = mul(UNITY_MATRIX_V, float4(o.worldPos, 1)).xyz * float3(-1, -1, 1);*/
				

				vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.screenPos = ComputeScreenPos(vertex); // grab position on screen
				UNITY_TRANSFER_FOG(o, vertex);

				/* testing reflections: */
				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 modelMatrixInverse = unity_WorldToObject;
				
				/*o.viewDir = mul(modelMatrix, v.vertex).xyz - _WorldSpaceCameraPos;
				o.normalDir = normalize(mul(float4(v.normal, 0.0), modelMatrixInverse).xyz);
				o.pos = UnityObjectToClipPos(v.vertex);*/

				


				return o;
			}

			sampler2D _CameraDepthTexture;
			fixed4 _Color;
			fixed3 _GlowColor;
			float _Fade;


			

			
			fixed4 frag (v2f i, UNITY_VPOS_TYPE vpos : VPOS) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) * _TintColor;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				fixed g = tex2D(_MainTex, i.uv).g + _TintColor;

				// rescale the range 0.2 .. 0.5 to 0.0 .. 1.0
				fixed desaturation = saturate((g - _WaterColor) / (_FoamColor - _WaterColor));

				// change 0.0 .. 0.5 .. 1.0 to 1.0 .. 0.0 .. 1.0
				desaturation = abs(desaturation * 2.0 - 1.0);

				

				// lerp 
				fixed3 cols = lerp(g * _TintColor.rgb, fixed3(g, g, g), desaturation);
				if (!IsGammaSpace())
				{
					cols = GammaToLinearSpace(cols);
				}

				col.a = cols + _GlowColor;

				// Make different color considering depth under water from camera angle:

				//half4 col = tex2D(_MainTex, i.uv) * _TintColor;// texture times tint;
				half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos))); // depth
				half4 foamLine = 0.6 - saturate(_Fade * (depth - i.screenPos.w)); // foam line by comparing depth and screenposition
				col += foamLine + _WaterColor; // add the foam line and tint to the texture*/




				// This stuff doesn't give me a good reflection alone
				/*float3 reflectedDir = reflect(i.viewDir, normalize(i.normalDir));
				col += texCUBE(_Cube, reflectedDir);*/


				return col;
			}
			ENDCG
		}
	}
}

