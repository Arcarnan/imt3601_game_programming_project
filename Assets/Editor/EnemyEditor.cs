﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(slimeAI))]
public class EnemyEditor : Editor
{
   public override void OnInspectorGUI()
   {
      base.OnInspectorGUI();

      slimeAI slime = (slimeAI)target;

      //GUILayout.BeginHorizontal
      if (GUILayout.Button("Generate color"))
      {
         Debug.Log("Pressed generate color button");
         slime.generateColor();
      }

      if (GUILayout.Button("Reset color"))
      {
         slime.ResetColor();
      }
   }
}
