﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Inventory : MonoBehaviour {
    
    public class ListItem
    {
        public string name { get; set; }
        public int amount { get; set; }

        public ListItem(string n, int a)
        {
            name = n;
            amount = a;
        }
    }
    private ItemDatabase itemDatabase;      // We can use itemDatabase.getItem(string name) to get prefab of item with that name
    public List<ListItem> itemInventory;     // List of items the user have.

    public GameObject inventoryHUD;
    

    void Start ()
    {

        QualitySettings.vSyncCount = 0;
        // Turn big inventory UI off to begin with.
        inventoryHUD.transform.Find("Inventory").gameObject.SetActive(false);

        itemDatabase = new ItemDatabase();
        itemDatabase.initPrefabs(); 

        itemInventory = new List<ListItem>();

        //TODO If the user has saved his inventory data in a file, those can be loaded from here:
        //itemInventory.Add("axe");

        setupHUD(); // Setup inventory HUD after items has been loaded.
    }

    void Update ()
    {
        updateHUD();// TODO make this one be called less often. :P
        if (Input.GetKeyDown(KeyCode.I))    // Open/Close inventory
        {
            Debug.Log("I got pressed");
            // Enable/disable UI for inventory:
            GameObject InventoryUI = inventoryHUD.transform.Find("Inventory").gameObject;
            if (InventoryUI.activeSelf)
                InventoryUI.SetActive(false);
            else
                InventoryUI.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.G))    // Throw away item on ground.
        {
            Transform handContainer = GameObject.Find("HandContainer").transform;
            try
            {
                //Get item:
                GameObject item = handContainer.transform.GetChild(0).gameObject;

                //Take item out of hands:
                item.transform.parent = null;

                // Put item on ground:
                item.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.43f, this.transform.position.z) + this.transform.forward * 0.5f + this.transform.right * 0.3f;

                // Enable stuff again:
                item.GetComponent<Collider>().enabled = true;
                item.transform.Find("ItemText").GetComponent<MeshRenderer>().enabled = true;
            } 
            catch
            {
                //Debug.Log("Exception throwing away item: " + e);
            }
        }

        // If player is holding anything, put it in the inventory:
        if (Input.GetKeyDown(KeyCode.Alpha0))   
        {
            putItemIn();
        }

        // Equip item in inventory spot nr 1.   
        if (Input.GetKeyDown(KeyCode.Alpha1))   
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be taken into inventory.
            try
            {
                putItemOut(itemInventory[0].name, 0);   // Puts item from inventory out to hands.
            } catch
            {
                // Don't do anything if there is items in inventory spot 0.
                Debug.Log("No item in inventory spot 0");
            }
        }
        // Equip item in inventory spot nr 2.   
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[1].name, 1);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 1.
                Debug.Log("No item in inventory spot 1");
            }
        }
        // Equip item in inventory spot nr 3.   
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[2].name, 2);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 2.
                Debug.Log("No item in inventory spot 2");
            }
        }
        // Equip item in inventory spot nr 4.
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[3].name, 3);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 3.
                Debug.Log("No item in inventory spot 3");
            }
        }
        // Equip item in inventory spot nr 5.
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[4].name, 4);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 4.
                Debug.Log("No item in inventory spot 4");
            }
        }
        // Equip item in inventory spot nr 6.
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[5].name, 5);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 5.
                Debug.Log("No item in inventory spot 5");
            }
        }
        // Equip item in inventory spot nr 7.
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[6].name, 6);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 6.
                Debug.Log("No item in inventory spot 6");
            }
        }
        // Equip item in inventory spot nr 8.
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[7].name, 7);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 7.
                Debug.Log("No item in inventory spot 7");
            }
        }
        // Equip item in inventory spot nr 9.   
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            putItemIn();  // If current item in hands is something else than item in spot nr 1, it will be removed.
            try
            {
                putItemOut(itemInventory[8].name, 8);   // Puts item from inventory out to hands.
            }
            catch
            {
                // Don't do anything if there is items in inventory spot 8.
                Debug.Log("No item in inventory spot 8");
            }
        }
    }

    void putItemOut(string itemName, int itemNr)   // Puts given item OUT of inventory into hands
    {
        // TODO check itemType, for different purposes. 
        try
        {
            
            Transform player = GameObject.Find("Player").transform;
            Vector3 position = player.position;
            Vector3 forward = player.forward;

            GameObject inventory = (GameObject)player.transform.Find("InventoryContainer").gameObject;  // Need to work more with inventory system...
            GameObject handObject = null;
            try // Get item in inventory, and set it into hands:
            {
                // Go through all items to find item with name == itemName:
                for (int i = 0; i < inventory.transform.childCount; i++)
                {
                    GameObject item = inventory.transform.GetChild(i).gameObject;
                    if (item.transform.Find("ItemText").GetComponent<TextMesh>().text == itemName)
                    {
                        handObject = (GameObject)item;
                        // If there is more than 1 amount of an item only "use up" 1 at a time.

                        //Get position of itemIn inventory with name:
                        if (itemInventory[itemNr].amount > 1)
                        {
                            itemInventory[itemNr].amount--;
                            break;
                        }
                        else
                        {
                            itemInventory.RemoveAt(itemNr); // Item is no longer in inventory when taking out of hands, if taking item back into inventory = put back in.
                            break;
                        }
                    }
                }

                handObject.transform.parent = GameObject.Find("HandContainer").transform;
                handObject.transform.position = player.position + 0.5f * player.forward + 0.39f * player.right + 0.15f * player.up;
                handObject.transform.forward = player.forward;
                // Make the object visible again:
                handObject.gameObject.SetActive(true);
            }
            catch
            {
                handObject = null;//hmmmTODO fix
                itemName = itemName + "(Clone)";
                // Go through all items to find item with name == itemName:
                for (int i = 0; i < inventory.transform.childCount; i++)
                {
                    GameObject item = inventory.transform.GetChild(i).gameObject;
                    if (item.transform.Find("ItemText").GetComponent<TextMesh>().text == itemName)
                    {
                        handObject = (GameObject)item;
                        // If there is more than 1 amount of an item only "use up" 1 at a time.
                        Debug.Log("name:" + itemInventory[i].name + " amount: " + itemInventory[i].amount);
                        if (itemInventory[itemNr].amount > 1)
                        {
                            itemInventory[itemNr].amount--;
                            break;
                        }
                        else
                        {
                            itemInventory.RemoveAt(itemNr); // Item is no longer in inventory when taking out of hands, if taking item back into inventory = put back in.
                            break;
                        }
                    }
                }
                
                handObject.transform.parent = GameObject.Find("HandContainer").transform;
                handObject.transform.position = player.position + 0.5f * player.forward + 0.39f * player.right + 0.15f * player.up;
                handObject.transform.forward = player.forward;
                // Make the object visible again:
                handObject.gameObject.SetActive(true);
            }
        }
        catch
        {
            Debug.Log("No item in spot nr 1.");
        }
    }

    void putItemIn()  // Puts any item INto inventory from hands.
    {
        Transform handContainer = GameObject.Find("HandContainer").transform;
        try
        {
            Transform item = handContainer.transform.GetChild(0);
            item.parent = GameObject.Find("InventoryContainer").gameObject.transform;
            item.gameObject.SetActive(false); // Make the object "invisible" as if it was in the backpack.

            // Set item into inventory again:

            // Add item to inventory:
            bool inList = false;

            // Check if already owning that kind of item:
            for (int i = 0; i < itemInventory.Count; i++) 
            {
                if (itemInventory[i].name == item.gameObject.transform.Find("ItemText").GetComponent<TextMesh>().text)
                {
                    inList = true;
                    itemInventory[i].amount += 1;
                    break;
                }
            }

            if (!inList)    // Add new item to inventory:
            {
                itemInventory.Add(new ListItem(item.gameObject.transform.Find("ItemText").GetComponent<TextMesh>().text, 1));  // Possibly add x amount when getting an item. default to 1.
            }

            item.GetComponent<Collider>().enabled = false;
        }
        catch
        {
            // No item in hands to remove, don't do anything then.
        }
    }
    
    void setupHUD() // If the player has items in inventory when starting the game they will be set in inventory HUD here.
    {
        // Get amount of inventoryUI slots to fill:
        int amountOfUISlots = inventoryHUD.transform.Find("Inventory").gameObject.transform.childCount;

        // loop through each of them and try-catch getting itemInventory[i].image:  
        for (int i = 0; i < amountOfUISlots; i++)
        {
            string itemName;
            int itemAmount;

            if (i < itemInventory.Count)
            {
                itemName = itemInventory[i].name;
                itemAmount = itemInventory[i].amount;
                
                Sprite currentImage = itemDatabase.getItem(itemName).itemImage;
                inventoryHUD.transform.Find("Inventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("ItemImage").GetComponent<Image>().sprite = currentImage;
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("Amount").GetComponent<Text>().text = itemAmount.ToString();
            }
        }
        
        // Setup items for the inventory HUD that shows all the time.
        for (int i = 0; i < 9; i++)
        {
            string itemName;
            int itemAmount;
            if (i < itemInventory.Count)
            {
                itemName = itemInventory[i].name;
                itemAmount = itemInventory[i].amount;

                Sprite currentImage = itemDatabase.getItem(itemName).itemImage;
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("ItemImage").GetComponent<Image>().sprite = currentImage;
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("Amount").GetComponent<Text>().text = itemAmount.ToString();
            }
        }
    }

    void updateHUD()    // add/remove items from inventory hud depending on what is in the itemInventory list.
    {
        // Get amount of inventoryUI slots to fill:
        int amountOfUISlots = inventoryHUD.transform.Find("Inventory").gameObject.transform.childCount;

        // loop through each of them and try-catch getting itemInventory[i].image:
        for (int i = 0; i < amountOfUISlots; i++)
        {
            string itemName;
            int itemAmount;
            if (i < itemInventory.Count)
            {
                itemName = itemInventory[i].name;
                itemAmount = itemInventory[i].amount;

                Sprite currentImage = itemDatabase.getItem(itemName).itemImage;
                inventoryHUD.transform.Find("Inventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("ItemImage").GetComponent<Image>().sprite = currentImage;
                inventoryHUD.transform.Find("Inventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("Amount").GetComponent<Text>().text = itemAmount.ToString();
            }
            else
            {
                inventoryHUD.transform.Find("Inventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("ItemImage").GetComponent<Image>().sprite = null;
                inventoryHUD.transform.Find("Inventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("Amount").GetComponent<Text>().text = "";
            }
        }
        
        // Setup items for the inventory HUD that shows all the time.
        for (int i = 0; i < 9; i++)
        {
            string itemName;
            int itemAmount;
            if (i < itemInventory.Count)
            {
                itemName = itemInventory[i].name;
                itemAmount = itemInventory[i].amount;

                Sprite currentImage = itemDatabase.getItem(itemName).itemImage;
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("ItemImage").GetComponent<Image>().sprite = currentImage;
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("Amount").GetComponent<Text>().text = itemAmount.ToString();
            }
            else
            {
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("ItemImage").GetComponent<Image>().sprite = null;
                inventoryHUD.transform.Find("SmallInventory").gameObject.transform.GetChild(i).gameObject.transform.Find("Border").gameObject.transform.Find("Amount").GetComponent<Text>().text = "";
            }
        }

    }

    void OnTriggerEnter(Collider other)   // for popping up text for itemName.
    {
        ItemPickup pickup = other.gameObject.transform.Find("ItemText").GetComponent<ItemPickup>();
        pickup.setTextActive(true);
    }

    void OnTriggerExit(Collider other)    // remove text popping up for itemName.
    {
        ItemPickup pickup = other.gameObject.transform.Find("ItemText").GetComponent<ItemPickup>();
        pickup.setTextActive(false);
    }

    void OnTriggerStay(Collider other)
    {
        // Gives user ability to pick up item:
        if (other.GetComponent<Collider>().gameObject.layer == LayerMask.NameToLayer("Item"))   
        {
            // Rotate text towards player:
            Vector3 targetDirection = other.gameObject.transform.position - transform.position;
            other.gameObject.transform.Find("ItemText").GetComponent<ItemPickup>().rotateText(targetDirection);

            if (Input.GetKeyDown(KeyCode.F))    // Pick up items with button F.
            {
                // Add item to inventory:
                bool inList = false;
                
                for (int i = 0; i < itemInventory.Count; i++)   // Check if already owning that kind of item:
                {
                    if (itemInventory[i].name == other.gameObject.transform.Find("ItemText").GetComponent<TextMesh>().text)
                    {
                        inList = true;
                        itemInventory[i].amount += 1;   
                    }
                }

                if (!inList)    // Add new item to inventory:
                {
                    itemInventory.Add(new ListItem(other.gameObject.transform.Find("ItemText").GetComponent<TextMesh>().text, 1));  // Possibly add x amount when getting an item. default to 1.
                }

                other.gameObject.transform.parent = GameObject.Find("InventoryContainer").gameObject.transform;   // Add item to inventory position.
                other.gameObject.SetActive(false);
                
                other.gameObject.GetComponent<Collider>().enabled = false; // Disable collider until item is not in inventory.
                other.gameObject.transform.Find("ItemText").GetComponent<MeshRenderer>().enabled = false; // Disable itemText.
            }
        }
    }

    public void onInventoryButtonPress(int buttonId)
    {
        Debug.Log("You pressed button: " + buttonId);
    }
}
