﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class slimeAI : MonoBehaviour
{

   public Transform player;

   public NavMeshAgent agent2;

   public GameObject[] waypoints;
   int currentWaypoint = 1;
   [Range(1, 10)]
   public float accuracyWaypoint = 2.5f;  // must be higher than enemy movement speed

   enum State { patrol, chase, returning };
   private State currentState = State.patrol;
   public bool randomPatrolRoute = true;

   // distance parameters for enemy behaviour
   [Range(45, 90)]
   public float fovAngle = 60;
   [Range(5, 20)]
   public int attentionDistance = 15;
   [Range(1, 20)]
   public int chaseDistance = 7;
   [Range(0.1f, 10.0f)]
   public float enemyMovementSpeed = 2.0f;
   [Range(1.0f, 3.0f)]
   public float rotSpeed = 0.5f;

   // Use this for initialization
   void Start()
   {
      //agent = player.GetComponent<UnityEngine.AI.NavMeshAgent>();
   }

   public void generateColor()
   {
      GetComponentInChildren<Renderer>().sharedMaterial.color = Random.ColorHSV();
   }

   public void ResetColor()
   {
      GetComponent<Renderer>().sharedMaterial.color = Color.green;
   }

   // Update is called once per frame
   void Update()
   {
      //////////////////////////////////  Debug distance representation
      Vector3 forward = transform.TransformDirection(Vector3.forward) * attentionDistance;
      Debug.DrawRay(transform.position, forward, Color.green);

      Vector3 forwardChase = transform.TransformDirection(Vector3.forward) * chaseDistance;
      Debug.DrawRay(transform.position, forwardChase, Color.red);

      //////////////////////////////////


      Vector3 direction = player.position - this.transform.position;
      direction.y = 0;

      float angle = Vector3.Angle(direction, this.transform.forward);      // check if player is within FoV of enemy

      if (currentState == State.patrol && waypoints.Length > 0)
      {

         if (Vector3.Distance(waypoints[currentWaypoint].transform.position, transform.position) < accuracyWaypoint)
         {
            if (randomPatrolRoute)
            {
               // for random patrol pattern
               currentWaypoint = Random.Range(0, waypoints.Length);
            }
            else
            {
               // for fixed patrol route pattern
               currentWaypoint++;
               if (currentWaypoint >= waypoints.Length)
               {
                  currentWaypoint = 0;
                  //Debug.Log("Waypoint array reset - " + waypoints[currentWaypoint], waypoints[currentWaypoint]);
               }
            }
         }

         agent2.SetDestination(waypoints[currentWaypoint].transform.position);
         /* manual movement without navmesh agent
         // rotate towards waypoint
         direction = waypoints[currentWaypoint].transform.position - transform.position;
         this.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
         this.transform.Translate(0, 0, Time.deltaTime * enemyMovementSpeed);
         */
      }


      if (Vector3.Distance(player.position, this.transform.position) < attentionDistance && (angle < fovAngle || currentState == State.chase))
      {
         currentState = State.chase;

         agent2.SetDestination(player.transform.position);
         /*manual movement without navmesh agent
          //rotate towards the player
         this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                      Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);

         //move forward if magnitude is within parameter
         if (direction.magnitude < chaseDistance)
         {
            this.transform.Translate(0, 0, enemyMovementSpeed * Time.deltaTime);
         }
         */
      }
      else if (Vector3.Distance(player.position, this.transform.position) >= attentionDistance && currentState == State.chase)
      {
         currentState = State.returning;
         currentWaypoint = 0;
         float closestWaypointSqr = Mathf.Infinity;
         Vector3 currentPosition = transform.position;

         // find closest waypoint after chasing player
         for (int i = 0; i < waypoints.Length; i++)
         {
            //Debug.Log(" i - " + i, null);

            Vector3 directionToWaypoint = waypoints[i].transform.position - currentPosition;
            float sqrToWaypoint = directionToWaypoint.sqrMagnitude;
            if (sqrToWaypoint < closestWaypointSqr)
            {
               closestWaypointSqr = sqrToWaypoint;
               currentWaypoint = i;
            }
         }
      }
      else
      {
         currentState = State.patrol;
      }
      //Debug.Log("closest waypoint - " + currentWaypoint, null);
   }
}
