﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase {

    // Prefabs of objects we want:
    private GameObject axePrefab;
    private GameObject swordPrefab;
    private Sprite swordImage;
    private Sprite axeImage;

    // This is a list of all items we have in the game which can be used.
    private List<Item> items = new List<Item>();

    // Initialize prefab into inventory or ItemDatabase.
    void initPrefab(ref GameObject item, string name, string storagePosition, ref Sprite image)
    {
        item = MonoBehaviour.Instantiate((GameObject)Resources.Load("Prefabs/" + name, typeof(GameObject)));
        item.transform.SetParent(GameObject.Find(storagePosition).transform);
        item.gameObject.SetActive(false);
        image = Resources.Load<Sprite>("Images/" + name);
    }
    
    // All item prefabs are set with their data here:
    public void initPrefabs()
    {
        initPrefab(ref axePrefab, "axe", "ItemDatabase", ref axeImage);
        initPrefab(ref swordPrefab, "sword", "ItemDatabase", ref swordImage);
        if (swordImage == null)
            Debug.Log("sword image is null");
    }

    public ItemDatabase ()
    {
        initPrefabs();
        // Name, power, type, prefab
        //items.Add(new Item("potion",    20,  Item.ItemType.consumable));
        //items.Add(new Item("antidote",  20,  Item.ItemType.consumable));
        //items.Add(new Item("key",       0,  Item.ItemType.consumable));

        items.Add(new Item("axe", 6, Item.ItemType.weapon, axePrefab, axeImage));
        items.Add(new Item("sword", 10, Item.ItemType.weapon, swordPrefab, swordImage));
        //items.Add(new Item("simple sword", 5, Item.ItemType.weapon, simpleSwordPrefab, simpleSwordImage));
        //items.Add(new Item("normal sword", 10, Item.ItemType.weapon));
        //items.Add(new Item("super sword", 20, Item.ItemType.weapon));
        //items.Add(new Item("bow",       50, Item.ItemType.weapon));
        //items.Add(new Item("arrow",     0,  Item.ItemType.ammo));
    }
   

    public Item getItem(string name)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemName == name)
            {
                return items[i];
            }
        }
        return null;
    }
}
